package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;

import org.json.JSONObject;

public class SetLocale extends BaseRequest {

	public SetLocale(Context context) {
		super(context);
	}

	/**
	 * get param
	 *
	 * @return
	 */
	public JSONObject getParam (String regionCode, String languageCode) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put(DB_REGION_CODE, regionCode);
			jobj.put(DB_LANGUAGE_CODE, languageCode);
			jobj.put("action","set");
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String regionCode, String languageCode, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_LOCALE, getParam(regionCode, languageCode), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			DataKeyUtil.setDBKey(mContext, DB_REGION_CODE, json.getString(DB_REGION_CODE));
			DataKeyUtil.setDBKey(mContext, DB_LANGUAGE_CODE, json.getString(DB_LANGUAGE_CODE));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
