package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class GetLocale extends BaseRequest {

	public GetLocale(Context context) {
		super(context);
	}

	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("action", "get");
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_LOCALE, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void request (final GetLocaleCallback apiCallback) {
		try {
			apiManager.call(API_GET_LOCALE, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					String regionCode = "";
					String languageCode = "";
					try
					{
						regionCode = json.getString(DB_REGION_CODE);
						languageCode = json.getString(DB_LANGUAGE_CODE);
					}
					catch (JSONException e)
					{
						e.printStackTrace();
					}
					if (CODE_SUCCESS.equals(code)) {
						DataKeyUtil.setDBKey(mContext, DB_REGION_CODE, regionCode);
						DataKeyUtil.setDBKey(mContext, DB_LANGUAGE_CODE, languageCode);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json, regionCode, languageCode);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			DataKeyUtil.setDBKey(mContext, DB_REGION_CODE, json.getString(DB_REGION_CODE));
			DataKeyUtil.setDBKey(mContext, DB_LANGUAGE_CODE, json.getString(DB_LANGUAGE_CODE));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public interface GetLocaleCallback
	{
		void response (String code, JSONObject json, String regionCode, String languageCode);
	}
}
