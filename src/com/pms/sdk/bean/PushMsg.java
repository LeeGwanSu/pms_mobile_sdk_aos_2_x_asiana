package com.pms.sdk.bean;

import android.os.Bundle;

import com.pms.sdk.IPMSConsts;

public class PushMsg implements IPMSConsts {

	public String msgId;
	public String msgGrpCd;
	public String notiTitle;
	public String notiMsg;
	public String notiImg;
	public String message;
	public String sound;
	public String msgType;
	public String data;

	public PushMsg(Bundle extras) {
		msgId = extras.getString(KEY_MSG_ID);
		msgGrpCd = extras.getString(KEY_MSG_GRP_CD);
		notiTitle = extras.getString(KEY_NOTI_TITLE);
		notiMsg = extras.getString(KEY_NOTI_MSG);
		notiImg = extras.getString(KEY_NOTI_IMG);
		message = extras.getString(KEY_MSG);
		sound = extras.getString(KEY_SOUND);
		msgType = extras.getString(KEY_MSG_TYPE);
		data = extras.getString(KEY_DATA);
	}

	@Override
	public String toString () {
		return String.format("onMessage:msgId=%s, msgGrpCd=%s, notiTitle=%s, notiMsg=%s, notiImg=%s, message=%s, sound=%s, msgType=%s, data=%s",
				msgId, msgGrpCd, notiTitle, notiMsg, notiImg, message, sound, msgType, data);
	}
}
